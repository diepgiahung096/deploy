FROM node:latest

MAINTAINER diepgiahung096@gmail.com

WORKDIR /apps

COPY ./insurance-portal-deploy/ .

EXPOSE 3000

ENTRYPOINT [ "npm","start" ]