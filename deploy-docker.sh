tokenGit=yFTWuZi2iqD_uBUX53bm
gitProject=insurance-portal-dev
deployProject=insurance-portal-deploy
gitUrl=https://diepgiahung096:$tokenGit@gitlab.com/diepgiahung096/$gitProject.git

echo -e "\n\t>>>>> Start pull <<<<<\n"
git clone $gitUrl
echo -e "\n\t-->>>>> Pull done <<<<<\n"
cd $gitProject
echo -e "\n\t-->>>>> Npm install && npm build <<<<<\n"
yarn install && yarn build
cd .. && mkdir $deployProject
mv $gitProject/public $gitProject/package.json $gitProject/.next $gitProject/node_modules $deployProject/ 
rm -rf $gitProject

echo -e "\n\t--->>>>> Run docker <<<<<---\n"
docker build -t $deployProject:v1 -f Dockerfile .
docker run --name $deployProject -p $publicPort:3000 -d $deployProject:v1
echo
docker ps | grep $deployProject:v1



