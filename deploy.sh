tokenGit=yFTWuZi2iqD_uBUX53bm
gitProject=insurance-portal-dev
deployProject=insurance-portal-deploy
export DEPLOY_HOME="$(pwd)"
sh=$(cd `dirname $DEPLOY_HOME` && pwd)
cd $sh
gitUrl=https://diepgiahung096:$tokenGit@gitlab.com/diepgiahung096/$gitProject.git

echo -e "\n\t>>>>> Start pull <<<<<\n"
git clone $gitUrl
cd $gitProject
echo -e "\n\t-->>>>> Pull done <<<<<\n"

echo -e "\n\t-->>>>> Npm install && npm build <<<<<\n"
yarn install && yarn build
cd $sh && mkdir $deployProject
mv $gitProject/public $gitProject/package.json $gitProject/.next $gitProject/node_modules $deployProject/ 
rm -rf $gitProject
cd $deployProject

echo -e "\n\t-->>>>> Start <<<<<--\n"
yarn start


